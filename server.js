const express = require('express');
const fs = require('fs');
const app = express();

const port = 3003;

let preData = fs.readFileSync('data.json');
let data = JSON.parse(preData);

app.get('/data', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.send(data);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
