import {
  CHANGE_FILTER_MODE,
  CHANGE_MIN_NUMBER,
  CHANGE_MAX_NUMBER,
  CHANGE_FIRST_LETTER,
  CHANGE_LAST_LETTER, GET_DATA_FROM_JSON
} from './actions';

const initialState = {
  filterMode: "default",
  minNumber: '',
  maxNumber: '',
  firstLetter: '',
  lastLetter: '',
  dataArray: []
};

export const rootReducer = (state = initialState, action) => {
  switch(action.type){
    case CHANGE_FILTER_MODE:
      return {...state, filterMode: action.payload};
    case CHANGE_MIN_NUMBER:
      return {...state, minNumber: action.payload};
    case CHANGE_MAX_NUMBER:
      return {...state, maxNumber: action.payload};
    case CHANGE_FIRST_LETTER:
      return {...state, firstLetter: action.payload};
    case CHANGE_LAST_LETTER:
      return {...state, lastLetter: action.payload};
    case GET_DATA_FROM_JSON:
      return {...state, dataArray: action.payload};
    default:
      return state;
  }
};
