import {
  CHANGE_FILTER_MODE,
  CHANGE_MIN_NUMBER,
  CHANGE_MAX_NUMBER,
  CHANGE_FIRST_LETTER,
  CHANGE_LAST_LETTER,
  GET_DATA_FROM_JSON
} from './actions';
import { rootReducer } from './reducers';

const initialTestState = {
  filterMode: "default",
  minNumber: '',
  maxNumber: '',
  firstLetter: '',
  lastLetter: '',
  dataArray: []
};

describe('Root Reducer', () => {

  it('Should render default state if we pass uncorrect params to rootReducer', () => {
    const newState = rootReducer(undefined, {});
    expect(newState).toEqual(initialTestState)
  });

  it('Should change state (filterMode: "byLetterRange") if action is CHANGE_FILTER_MODE', () => {
    const testPayload = "byLetterRange";
    const newState = rootReducer(undefined, {
      type: CHANGE_FILTER_MODE,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      filterMode: "byLetterRange",
    })
  });

  it('Should change state (filterMode: "byLetterRange") if action is CHANGE_FILTER_MODE', () => {
    const testPayload = "byLetterRange";
    const newState = rootReducer(undefined, {
      type: CHANGE_FILTER_MODE,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      filterMode: "byLetterRange",
    })
  });

  it('Should change state (minNumber: 10) if action is CHANGE_MIN_NUMBER', () => {
    const testPayload = 10;
    const newState = rootReducer(undefined, {
      type: CHANGE_MIN_NUMBER,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      minNumber: 10,
    })
  });

  it('Should change state (maxNumber: 10) if action is CHANGE_MAX_NUMBER', () => {
    const testPayload = 50;
    const newState = rootReducer(undefined, {
      type: CHANGE_MAX_NUMBER,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      maxNumber: 50,
    })
  });

  it(`Should change state (firstLetter: 'B') if action is CHANGE_FIRST_LETTER`, () => {
    const testPayload = 'B';
    const newState = rootReducer(undefined, {
      type: CHANGE_FIRST_LETTER,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      firstLetter: 'B',
    })
  });

  it(`Should change state (lastLetter: 'W') if action is CHANGE_LAST_LETTER`, () => {
    const testPayload = 'W';
    const newState = rootReducer(undefined, {
      type: CHANGE_LAST_LETTER,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      lastLetter: 'W',
    })
  });

  it(`Should fill empty dataArray with objects if action is GET_DATA_FROM_JSON`, () => {
    const testPayload = [{
      "firstName": "Katsiaryna",
      "lastName": "Kvitkevish",
      "age": 50,
      "streetAddress": "40 Biaduli",
      "city": "Minsk",
      "phoneNumbers": "+375296988529",
      "description": "Lorem test test test"
    }];
    const newState = rootReducer(undefined, {
      type: GET_DATA_FROM_JSON,
      payload: testPayload
    });
    expect(newState).toEqual({
      ...initialTestState,
      dataArray: [...testPayload],
    })
  });

});
