import React from 'react';
import './MainPage.scss';
import Table from './../Table/Table';
import FilterTextGroup from './../FilterTextGroup/FilterTextGroup';
import FilterNumberGroup from './../FilterNumberGroup/FilterNumberGroup';

class MainPage extends React.Component {
  render(){
    return (
      <div className="MainPage" data-test="MainPage-wrapper">
        <FilterTextGroup />
        <FilterNumberGroup />
        <Table />
      </div>
    )
  };
}

export default MainPage;
