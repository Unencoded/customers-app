import React from 'react';
import { shallow } from 'enzyme';
import MainPage from './MainPage';
import FilterNumberGroup from './../FilterNumberGroup/FilterNumberGroup';
import FilterTextGroup from './../FilterTextGroup/FilterTextGroup';
import Table from './../Table/Table';

const setUp = () => {
  const component = shallow(<MainPage />);
  return component;
};

describe('MainPage component', () => {

  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('Always should display FilterNumberGroup component', () => {
    const wrapper = component.find(FilterNumberGroup);
    expect(wrapper.length).toBe(1);
  });

  it('Always should display FilterTextGroup component', () => {
    const wrapper = component.find(FilterTextGroup);
    expect(wrapper.length).toBe(1);
  });

  it('Always should display Table component', () => {
    const wrapper = component.find(Table);
    expect(wrapper.length).toBe(1);
  });

  it('Always should display wrapper div', () => {
    const wrapper = component.find(`[data-test='MainPage-wrapper']`);
    expect(wrapper.length).toBe(1);
  });

});
