import React from 'react';
import { shallow } from 'enzyme';
import Table from './../Table/Table';
import { findByTestAtrr, testStore } from '../../../Utils';
import UserRow from '../UserRow/UserRow';

//shallowed component with connected temporary store
const setUp = (testState={}) => {
  const store = testStore(testState);
  const wrapper = shallow(<Table store={store}/>).childAt(0).dive();
  return wrapper;
};

describe('Table component', () => {

  let wrapperDefault, wrapperNumberRange, wrapperLetterRange;
  beforeEach(() => {
    //temporary store for testing
    const testDataArray = [
      {
        "firstName": "Avril",
        "lastName": "Avitkevich",
        "age": 20,
        "streetAddress": "40 Biaduli",
        "city": "Minsk",
        "phoneNumbers": "+375296988529",
        "description": "Lorem test test test"
      },
      {
        "firstName": "Zmitrok",
        "lastName": "Ziadulia",
        "age": 65,
        "streetAddress": "20 Biaduli",
        "city": "Slonim",
        "phoneNumbers": "+3752969529",
        "description": "Lorem test test test"
      }
      ];

    const initialState = {
      filterMode: "default",
      minNumber: '30',
      maxNumber: '70',
      firstLetter: '',
      lastLetter: '',
      dataArray: testDataArray
    };

    const stateNumberRange = {
      filterMode: "byNumberRange",
      minNumber: '30',
      maxNumber: '70',
      firstLetter: '',
      lastLetter: '',
      dataArray: testDataArray
    };

    const stateLetterRange = {
      filterMode: "byLetterRange",
      minNumber: '',
      maxNumber: '',
      firstLetter: 'A',
      lastLetter: 'T',
      dataArray: testDataArray
    };

    wrapperDefault = setUp(initialState);
    wrapperNumberRange = setUp(stateNumberRange);
    wrapperLetterRange = setUp(stateLetterRange);
  });

  it('Should display 1 UserRow if filterMode is byNumberRange', () => {
    const component = wrapperNumberRange.find(UserRow);
    expect(component.length).toBe(1);
  });

  it('Should display 1 UserRow if filterMode is byLetterRange', () => {
    const component = wrapperLetterRange.find(UserRow);
    expect(component.length).toBe(1);
  });

  it('Should display 2 UserRow(s)', () => {
    const component = wrapperDefault.find(UserRow);
    expect(component.length).toBe(2);
  });

  it('Always should display table', () => {
    const component = findByTestAtrr(wrapperDefault, 'table');
    expect(component.length).toBe(1);
  });

  it('Always should display table header', () => {
    const component = findByTestAtrr(wrapperDefault, 'table-row-head');
    expect(component.length).toBe(1);
  });

  it('Should display one or more UserRow(s) if initialState.dataArray is not empty', () => {
    const component = wrapperDefault.find(UserRow);
    expect(component.length).not.toBe(0);
  });

});
