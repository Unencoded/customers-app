import React from 'react';
import './Table.scss';
import { connect } from 'react-redux';
import UserRow from '../UserRow/UserRow'

class Table extends React.Component{

  mappedData (){
    let sortedData;

    switch(this.props.currentDataMode){
      case "byNumberRange":
        let minAge = this.props.currentMinNumber || 0;
        let maxAge = this.props.currentMaxNumber || 99;

        sortedData = this.props.arrayFromJSON.map( (item, index) => {
          if (item.age > minAge && item.age < maxAge) {
            // return mappedRow(item, index)
            return <UserRow item={item} index={index} key={item+index}/>
          } return null
          });
        break;
      //sort list by letter range form firstLetter to lastLetter
      case "byLetterRange":
        let firstLetter = this.props.currentFirstLetter.toUpperCase()[0] || 'A';
        let lastLetter = this.props.currentLastLetter.toUpperCase()[0] || 'Z';

        sortedData = this.props.arrayFromJSON.map( (item, index) => {
          if (item.lastName.toUpperCase()[0] >= firstLetter && item.lastName.toUpperCase()[0] <= lastLetter) {
            return <UserRow item={item} index={index} key={item+index}/>
          } return null
        });
        break;
      default:
        sortedData = this.props.arrayFromJSON.map( (item, index) => {
          return <UserRow item={item} index={index} key={item+index}/>
        });
    }
    return sortedData;
  };

  render(){
    return(
      <div className="table" data-test="table">
        <div className="table-row head" data-test="table-row-head">
          <div className="table-td">firstName</div>
          <div className="table-td">lastName</div>
          <div className="table-td">age</div>
          <div className="table-td">streetAddress</div>
          <div className="table-td">city</div>
          <div className="table-td">phoneNumbers</div>
        </div>
        {this.mappedData()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentDataMode: state.filterMode,
    currentMinNumber: state.minNumber,
    currentMaxNumber: state.maxNumber,
    currentFirstLetter: state.firstLetter,
    currentLastLetter: state.lastLetter,
    arrayFromJSON: state.dataArray,
  }
};

export default connect(mapStateToProps)(Table);
