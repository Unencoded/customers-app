import React from 'react';
import { shallow } from 'enzyme';
import FilterNumberGroup from './../FilterNumberGroup/FilterNumberGroup';
import { findByTestAtrr, testStore } from '../../../Utils';

const setUp = (initialState={}) => {
  const store = testStore(initialState);
  const wrapper = shallow(<FilterNumberGroup store={store} />).childAt(0).dive();
  return wrapper;
};

describe('FilterNumberGroup component', () => {

  let wrapper;
  beforeEach(() => {
    const initialState = {
      filterMode: "default",
      minNumber: '888',
      maxNumber: '',
      firstLetter: '',
      lastLetter: '',
      dataArray: [{
        "firstName": "Katsiaryna",
        "lastName": "Kvitkevish",
        "age": 50,
        "streetAddress": "40 Biaduli",
        "city": "Minsk",
        "phoneNumbers": "+375296988529",
        "description": "Lorem test test test"
      }]
    };
    wrapper = setUp(initialState);
  });

  it('Always should display first input', () => {
    const component = findByTestAtrr(wrapper, 'first-number-input');
    expect(component.length).toBe(1);
  });

  it('Always should display second input', () => {
    const component = findByTestAtrr(wrapper, 'second-number-input');
    expect(component.length).toBe(1);
  });

});
