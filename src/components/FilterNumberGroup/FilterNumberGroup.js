import React from 'react';
import './FilterNumberGroup.scss';
import { CHANGE_MIN_NUMBER, CHANGE_MAX_NUMBER, CHANGE_FILTER_MODE } from '../../store/actions';
import { connect } from 'react-redux';

function FilterNumberGroup(props){
  let val = (event) => +event.target.value;

  return (
    <div className="FilterNumberGroup" data-test="FilterNumberGroup-wrapper">
      Show customers whose <b>age</b> in the range from:
      <input type="number"
             data-test="first-number-input"
             placeholder={'0'}
             onChange={(e) => {
               props.changeMinNumber(val(e) < 0 || val(e) > 99 ? '' : val(e));
               props.changeFilterMode("byNumberRange");
             }}
             value={props.currentMinNumber}
      />
      to
      <input type="number"
             data-test="second-number-input"
             placeholder={'99'}
             onChange={(e) => {
               props.changeMaxNumber(val(e) > 99 ? '' : val(e));
               props.changeFilterMode("byNumberRange");
             }}
             value={props.currentMaxNumber}
      />
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    currentMinNumber: state.minNumber,
    currentMaxNumber: state.maxNumber,
  }
};

const putActionsToProps = (dispatch) => {
  return{
    changeFilterMode: (newFilterMode) => dispatch({
      type: CHANGE_FILTER_MODE,
      payload: newFilterMode
    }),
    changeMinNumber: (newMinNumber) => dispatch({
      type: CHANGE_MIN_NUMBER,
      payload: newMinNumber
    }),
    changeMaxNumber: (newMaxNumber) => dispatch({
      type: CHANGE_MAX_NUMBER,
      payload: newMaxNumber
    })
  }
};

export default connect(mapStateToProps, putActionsToProps)(FilterNumberGroup);
