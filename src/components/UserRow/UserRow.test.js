import React from 'react';
import Table from './../Table/Table';
import { shallow } from 'enzyme';
import { findByTestAtrr, testStore } from '../../../Utils';

const setUp = (initialState = {}) => {
  const store = testStore(initialState);
  // getting UserRow component through the Table component. Because we need Table be connected to store and have at least one row. Then make double .childAt(0).dive()  to get our UserRow
  const shallowedWrapper = shallow(<Table store={store} />).childAt(0).dive().childAt(1).dive();
  return shallowedWrapper;
};

describe('UserRow component', () => {

  let wrapper;
  beforeEach(() => {
    const initialState = {
      filterMode: "default",
      minNumber: '0',
      maxNumber: '99',
      firstLetter: 'A',
      lastLetter: 'Z',
      dataArray: [{
        "firstName": "Katsiaryna",
        "lastName": "Kvitkevish",
        "age": 50,
        "streetAddress": "40 Biaduli",
        "city": "Minsk",
        "phoneNumbers": "+375296988529",
        "description": "Lorem test test test"
      },
      ]
    };
    wrapper = setUp(initialState);

  });

  it('should display 6 table-td elements', () => {
    const component = findByTestAtrr(wrapper, 'table-td');
    expect(component.length).toBe(6);
  });

});
