import React from 'react';
import { Link } from 'react-router-dom';

function UserRow(props) {
  return (
    <Link to={`/${props.item.lastName + props.index}`} className="table-row" key={props.index}>
      <div className="table-td" data-test="table-td">{props.item.firstName}</div>
      <div className="table-td" data-test="table-td">{props.item.lastName}</div>
      <div className="table-td" data-test="table-td">{props.item.age}</div>
      <div className="table-td" data-test="table-td">{props.item.streetAddress}</div>
      <div className="table-td" data-test="table-td">{props.item.city}</div>
      <div className="table-td" data-test="table-td">{props.item.phoneNumbers}</div>
    </Link>
  )
};

export default UserRow;
