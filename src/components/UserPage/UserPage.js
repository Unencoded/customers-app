import React from 'react';
import {
  Route,
} from "react-router-dom";
import { connect } from 'react-redux';
import './UserPage.scss';

function UserPage(props){
  return (
  props.arrayFromJSON.map( (data, index) => {
    return (
      // it opens separate page with full information (with description)
      <Route key={data.lastName + index} path={`/${data.lastName + index}`}>
        <div className="UserPage-wrapper">
          <h1 data-test='userPage-h1'>Personal data of user {`${data.firstName + ' ' + data.lastName}`}</h1>
          <ul>
            <li data-test="user-data-li"><b>First name:</b> {data.firstName}</li>
            <li data-test="user-data-li"><b>Last name:</b> {data.lastName}</li>
            <li data-test="user-data-li"><b>Age:</b> {data.age}</li>
            <li data-test="user-data-li"><b>Street address:</b> {data.streetAddress}</li>
            <li data-test="user-data-li"><b>City:</b> {data.city}</li>
            <li data-test="user-data-li"><b>Phone number:</b> {data.phoneNumbers}</li>
            <li data-test="user-data-li"><b>Description:</b> {data.description}</li>
          </ul>
        </div>
      </Route>
    )
  }))};

const mapStateToProps = (state) => {
  return {
    arrayFromJSON: state.dataArray,
  }
};

export default connect(mapStateToProps)(UserPage);
