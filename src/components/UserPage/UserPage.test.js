import React from 'react';
import UserPage from './UserPage';
import { shallow } from 'enzyme';
import { testStore } from '../../../Utils';

const setUp = (initialState = {}) => {
  const store = testStore(initialState);
  const shallowedWrapper = shallow(<UserPage store={store}/>).childAt(0).dive();
  return shallowedWrapper;
};

describe('UserPage component', () => {

  let wrapper;
  beforeEach(() => {
    const initialState = {
      filterMode: "default",
      minNumber: '0',
      maxNumber: '99',
      firstLetter: 'A',
      lastLetter: 'Z',
      dataArray: [{
        "firstName": "Katsiaryna",
          "lastName": "Kvitkevish",
          "age": 50,
          "streetAddress": "40 Biaduli",
          "city": "Minsk",
          "phoneNumbers": "+375296988529",
          "description": "Lorem test test test"
      }]
    };
    wrapper = setUp(initialState);
  });

  it('Always should display h1', () => {
    expect(wrapper.find(`[data-test='userPage-h1']`)).toHaveLength(1);
  });

  it('Always should display 7 elements of the list', () => {
    expect(wrapper.find(`[data-test='user-data-li']`)).toHaveLength(7);
  });

});
