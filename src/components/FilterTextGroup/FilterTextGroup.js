import React from 'react';
import './FilterTextGroup.scss';
import { connect } from 'react-redux';
import {
  CHANGE_FILTER_MODE,
  CHANGE_FIRST_LETTER,
  CHANGE_LAST_LETTER
} from './../../store/actions';

function FilterTextGroup(props){
  return (
    <div className="FilterTextGroup">
      Show customers whose <b>last name </b> is in diapason from
      <input type="text"
             placeholder={'A'}
             data-test="first-text-input"
             maxLength="1"
             onChange={(event) => {
               props.changeFirstLetter(event.target.value);
               props.changeFilterMode("byLetterRange")
             }}
             value={ props.currentFirstLetter.toUpperCase() }
      />
      to
      <input type="text"
             placeholder={'Z'}
             data-test="second-text-input"
             maxLength="1"
             onChange={(event) => {
               props.changeLastLetter(event.target.value);
               props.changeFilterMode("byLetterRange")
             }}
             value={ props.currentLastLetter.toUpperCase() }
      />
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    currentFirstLetter: state.firstLetter,
    currentLastLetter: state.lastLetter,
  }
};

const putActionToProps = (dispatch) => {
  return {
    changeFilterMode: (newFilterMode) => dispatch({
      type: CHANGE_FILTER_MODE,
      payload: newFilterMode
    }),
    changeFirstLetter: (newFirstLetter) => dispatch({
      type: CHANGE_FIRST_LETTER,
      payload: newFirstLetter
    }),
    changeLastLetter: (newLastLetter) => dispatch({
      type: CHANGE_LAST_LETTER,
      payload: newLastLetter
    })
  };
};

export default connect(mapStateToProps, putActionToProps)(FilterTextGroup);