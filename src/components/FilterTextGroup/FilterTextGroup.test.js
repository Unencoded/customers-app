import React from 'react';
import { shallow } from 'enzyme';
import FilterTextGroup from './../FilterTextGroup/FilterTextGroup';
import { findByTestAtrr, testStore } from '../../../Utils';

const setUp = (initialState={}) => {
  const store = testStore(initialState);
  const wrapper = shallow(<FilterTextGroup store={store} />).childAt(0).dive();
  return wrapper;
};

describe('FilterTextGroup component', () => {

  let wrapper;
  beforeEach(() => {
    const initialState = {
      filterMode: "default",
      minNumber: '',
      maxNumber: '',
      firstLetter: '',
      lastLetter: '',
      dataArray: [{
        "firstName": "Katsiaryna",
        "lastName": "Kvitkevish",
        "age": 50,
        "streetAddress": "40 Biaduli",
        "city": "Minsk",
        "phoneNumbers": "+375296988529",
        "description": "Lorem test test test"
      }]
    };
    wrapper = setUp(initialState);
  });

  it('Always should display first text input', () => {
    const component = findByTestAtrr(wrapper, 'first-text-input');
    expect(component.length).toBe(1);
  });

  it('Always should display second text input', () => {
    const component = findByTestAtrr(wrapper, 'second-text-input');
    expect(component.length).toBe(1);
  });

});
