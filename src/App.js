import React from 'react';
import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import MainPage from './components/MainPage/MainPage';
import { GET_DATA_FROM_JSON } from './store/actions';
import { connect } from 'react-redux';
import UserPage from './components/UserPage/UserPage';

class App extends React.Component {
  componentDidMount(){
    fetch("http://localhost:3003/data")
      .then(res => res.json())
      .then(result => {
        //put our data-array to the store
        return this.props.fillDataArray(result)
      })
      .catch(() => {
        //if something wrong with getting data from server shows alert
        alert('Request to server is failed! Cannot fill the table with data...')
    });
  }

  render(){
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route path="/" exact component={MainPage} />
            <UserPage />
          </Switch>
        </div>
      </Router>
    )
  };
}

const putActionToProps = (dispatch) => {
  return {
    fillDataArray: (newArrayFromJSON) => dispatch({
      type: GET_DATA_FROM_JSON,
      payload: newArrayFromJSON
    })
  };
};

export default connect(null, putActionToProps)(App);
